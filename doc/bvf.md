# Linux bridge vlan filtering
_The following explanations are extracted and adapted from [developers.redhat.com](https://developers.redhat.com/blog/2017/09/14/vlan-filter-support-on-bridge#with_vlan_filtering)_

The Linux bridge is actually a virtual switch and widely used with KVM/QEMU hypervisor.  
VLAN is another very important function in virtualization. It allows network administrators to group hosts together even if the hosts are not on the same physical network switch. This can greatly simplify network design and deployment. Also, it can separate hosts/guests under the same switch/bridge into different subnets.
## Without Vlan filtering
Previously, if we wanted to use distinct subnets with guests on a virtualization server, we need to create multiple VLANs and bridges. Something like:

![schema2](pics/schema2.png?raw=true "schema_2")

## With Vlan filtering
Linux 3.9 introduced the ability to use VLAN filtering directly on bridge interface:

![schema3](pics/schema3.png?raw=true "schema_3")  
With VLAN filtering capabilities, the Linux bridge acts more like a real switch now. We don't need to create multiple VLANs and bridges anymore.  
The topology appears easier to manage: distinct subnets can be add more easily and efficiently.

## Manual configuration

LVBC use linux bridge vlan filtering capabilities and proceeds to the above configuration automatically.  
This avoids having to do the manual steps detailed below each times guests are start / stop:

1. Create the bridge interface and enable VLAN filtering
    ```
    # ip link add br0 type bridge
    # ip link set br0 up
    # ip link set br0 type bridge vlan_filtering 1
    # ip link set eth0 master br0
    ```
2. Attach tap devices to the bridge
    ```
    # ip link set guest_1_tap_0 master br0
    # ip link set guest_2_tap_0 master br0
    # ip link set guest_2_tap_1 master br0
    # ip link set guest_3_tap_0 master br0
    ```
3. Set Vlans on taps interfaces and on physical eth0 interface
    ```
    # bridge vlan add dev guest_1_tap_0 vid 2 pvid untagged master
    # bridge vlan add dev guest_2_tap_0 vid 2 pvid untagged master
    # bridge vlan add dev guest_2_tap_1 vid 3 pvid untagged master
    # bridge vlan add dev guest_3_tap_0 vid 3 pvid untagged master

    # bridge vlan add dev eth0 vid 2 master
    # bridge vlan add dev eth0 vid 3 master
    ```
4. Dump VLAN infos from the bridge interface
    ```
    # bridge vlan show

    port            vlan ids
    eth0            1 PVID Egress Untagged
                    2
                    3
    
    br0             1 PVID Egress Untagged
    
    guest_1_tap_0   1 Egress Untagged
                    2 PVID Egress Untagged

    guest_2_tap_0   1 Egress Untagged
                    2 PVID Egress Untagged

    guest_2_tap_1   1 Egress Untagged
                    3 PVID Egress Untagged

    guest_3_tap_0   1 Egress Untagged
                    3 PVID Egress Untagged
    ```
