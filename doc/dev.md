# Information for developers
## Big picture

Beginning with libvirt 0.8.0, specific events on a host system will trigger custom scripts thanks to the [libvirt hook mecanism](https://libvirt.org/hooks.html).  

Following hooks can be triggered:
- The libvirt daemon starts, stops, or reloads its configuration  ( since 0.8.0 )
- A QEMU guest is started or stopped ( since 0.8.0 )
- An LXC guest is started or stopped ( since 0.8.0 )
- A libxl-handled Xen guest is started or stopped ( since 2.1.0 )
- A network is started or stopped or an interface is plugged/unplugged to/from the network ( since 1.2.2 )  

Nevertheless, libvirt hooks suffer from the following limitations:
- A hook script is triggered only in certain situations provided by libvirt
- An error in the hook script will result in the guest not starting
- The hooks will be used synchronously with the launch of a guest
- A hook script must not call back into libvirt, as the libvirt daemon is already waiting for the script to exit. A deadlock is likely to occur.

LVBC uses libvirt hooks but introduces a communication between the hook script (the general input program lvbhook) and the LVBC daemon through a Unix socket.
This is summarized in the following diagram:  

![schema6](pics/schema6.png?raw=true "schema_6")  

LVBC design advantages:
- lvbhook can be written in such a way that it does not generate errors and therefore gives priority to libvirt to launch guests
- lvbhook can be written to release libvirtd as quickly as possible in an asynchronous way, leaving LVBC daemon to work quietly
- nothing prevents the LVBC daemon to talk directly with libvirt if needed (no risk of deadlock)
- LVBC can react to all kinds of events planned by the developer (like reloading the configuration) without depending only on the events planned in the libvirt hooks
- it is possible to use a finest security policy on system by delegating the writing rights on the lvbc configuration files for an admin user of the virtual machines without this one being root of the hypervisor (typically a user member of the group libvirt-qemu).

## Go version
LVBC use [netlink](https://github.com/vishvananda/netlink) library for network configuration. Netlink is the interface that a linux user-space program uses to communicate with the kernel.  
All Go versions above 1.12 can be used for LVBC.
## Beeing root for testing your code
Netlink communication requires elevated privileges, so in most cases this code needs to be run as root which can be annoying when you code.  
it is nevertheless possible to use the following commands to make the process a little easier (requires the installation of sudo):  
```bash
# for testing in specific package
sudo -E env "PATH=$PATH" go test 
# for running
sudo -E env "PATH=$PATH" go run cmd/lvbc/main.go 
```
## Specific paths during dev
LVBC uses in its production version the following path for the communication socket: `/var/run/lvbc.sock` as well as the directory `/usr/local/etc/lvbc` for the json bridges configuration files.  
A more practical configuration is used during development (i.e. as long as you do not produce binaries with the provided Makefile).  
socket: `/tmp/lvbc.sock` and lvbc config files: `./bridges`
## Produce your own binaries / package
Uses the provided Makefile
```
make clean
make build or make dist
```
## Using laptop for development
Not everyone has a machine with several network cards (minimum two cards) to develop. However, if you have like me a simple laptop, you can use your wifi card to guarantee a connection to the internet (and simulate the behavior of the infra network described in the schematics) and use the wired card for the bridge.  
However, do not reverse because mounting a bridge on a wifi interface is something a little bit specific.

## Apparmor by-pass during dev
For debian users, an apparmor profile is used by default. This profile limits the hook scripts (so the lvbhook program) in the following directory:
`/etc/libvirt/hooks`  
here is the line concerned in the apparmor profile:
```bash
$ cat /etc/apparmor.d/usr.sbin.libvirtd | grep -n hooks
104: /etc/libvirt/hooks/** rmix,
```
For the development version, qemu is linked to lvbhook in the designated directory `/etc/libvirt/hooks` so this is not a particular problem.  
However, when developing, it is not practical to build lvbhook and link to qemu in the good directory each time.
One way to do it it's to link `/etc/libvirt/hooks/qemu` to your run time lvbhook program and put libvirt apparmor profile in complain mode during dev process (don't forget to put apparmor in strict mode when you will have finished developing.)
```bash
# build lvbhook
go build -o <lvbc_repo>/cmd/lvbhook/lvbhook <lvbc_repo>/cmd/lvbhook/main.go
# link qemu hook on your lvbhook prog
ln -s /<lvbc_repo>/cmd/lvbhook/lvbhook /etc/libvirt/hooks/qemu
# put libvirt in complain mode
sudo aa-complain /usr/sbin/libvirtd
# just rebuild lvbhook during your dev process.
```
## Docker special warning
I would guess that if you are developing on your machine, you probably also have a docker daemon running somewhere.  
**The best thing you can do is to disable docker for the time of development and reboot your machine.**
```
sudo systemctl stop containerd.service
sudo systemctl disable containerd.service
sudo systemctl stop docker.service
sudo systemctl disable docker.service
sudo systemctl stop docker.socket
sudo systemctl disable docker.socket
sudo reboot
```
### why ?
Well, you may not know it but using docker is not without consequences on your iptables rules and especially with the rules applying to bridges (docker uses bridges).  
Try to do an `iptables-save` on your machine when docker is running with few containers and you will be surprised how many rules were automatically set by docker.  
Some of these rules can be blocking (filter table, forward chains) for the bridges used by libvirt and leaving you in incomprehension on the origin of the bridge dysfunction.  
Add to this that docker automatically configures your system for bridges to use iptables, ip6tables and arp_tables with the following global settings:
```bash
$ cat /proc/sys/net/bridge/bridge-nf-call-iptables
1
$ cat /proc/sys/net/bridge/bridge-nf-call-ip6tables 
1
$ cat /proc/sys/net/bridge/bridge-nf-call-arptables 
1
```
In this case the local configurations made later on the birdges by LVBC ( special sys entries in `/sys/class/net/br0/bridge/...`) will have no guaranteed effects.  
**So you have been warned, never ever use docker on a hypervisor and especially in a production context !**  