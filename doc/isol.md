# Proper isolation for Linux Bridge
## Reference
There is a [good post](https://vincent.bernat.ch/en/blog/2017-linux-bridge-isolation) on the subject and I encourage you to read.  
The author [Vincent Bernat](https://vincent.bernat.ch/en) always makes precise and well documented analyses in all his articles and [projects](https://github.com/vincentbernat) which are a real gold mine for those who are interested in linux kernel, code and network stuff. 

## Use case
![schema5](pics/schema_5.png?raw=true "schema_5")  

The main expectation of such a setup is that while the virtual hosts should be able to use resources from the users network, they should not be able to access resources from the infrastructure network (including resources hosted on the hypervisor itself).  
**If you do not take special precautions, this will not be the case:**
1. Create the bridge interface and enable VLAN filtering
    ```
    # ip link add br0 type bridge
    # ip link set br0 up
    # ip link set br0 type bridge vlan_filtering 1
    ```
2. Attach devices to the bridge
    ```
    # ip link set eth1 master br0
    # ip link set tap0 master br0
    ```

3. Dump VLAN infos from the bridge interface
    ```
    # bridge vlan show
        port            vlan ids
        eth1            1 PVID Egress Untagged
        br0             1 PVID Egress Untagged
        tap0            1 PVID Egress Untagged
    ```
4. Set address on guest interface
    ```
    ip address add 10.0.0.1/8 dev eth0
    ```

5. Add a specific route on guest interface and try to ping hypervisor host
    ```
    ip route add 192.168.100.1 dev eth0
    ping 192.168.100.1
    ```
6. On the hypervisor side, listen icmp messages
    ```
    #tcpdump -n -v icmp
    tcpdump: listening on eth0, link-type EN10MB (Ethernet), capture size 262144 bytes
    10:55:13.567937 IP (tos 0x0, ttl 64, id 60915, offset 0, flags [none], proto ICMP (1), length 84)
    192.168.100.1 > 10.0.0.1: ICMP echo reply, id 3113, seq 21, length 64
    10:55:14.568288 IP (tos 0x0, ttl 64, id 61026, offset 0, flags [none], proto ICMP (1), length 84)
    192.168.100.1 > 10.0.0.1: ICMP echo reply, id 3113, seq 22, length 64
    ```
    **So we can clearly see that the isolation between the guest and the hypervisor is not provided properly.**

## Origins
The explanations of this behavior are exposed in the [article](#reference) quoted above.  
It is also important to specify linux bridges relation to the iptables stack.  

Linux' bridge filter framework has available mechanisms where the layer 2 bridge code can do an upcall to iptables (as well as arptables or ip6tables) and have filtering travel from layer 2 (bridged frames) through layer 3 (iptables with packets) and then back to layer 2. This is much beyond the use the BROUTING chain which only gives the logical choice of staying at layer 2 or continuing at layer 3 (by doing a frame dnat/broute to local).
This layering violation allows for example to leverage the conntrack facility and have stateful firewalling available at layer 2.  
**It also caused troubles when people didn't expect this to happen and got issues hard to debug, introducing security flow or hindered performances when it was (most of the time) not needed.**  
Read the [libvirt wiki and related discussion](https://wiki.libvirt.org/Net.bridge.bridge-nf-call_and_sysctl.conf.html) on this subject.

## VID 1 in linux bridge
When using the vlan filtering bridge, vlan1 is automatically added to all interfaces connected to this bridge.
This is similar to the behavior found on physical switchs where vlan1, sometimes referred to as the native vlan, allows an unconfigured interface to be assigned to a default vlan.  
Packet filtering related to vlan membership is done really early in the packet's journey through the network stack.  
**Also, it is possible to work on this property with vlan 1 to perform a correct isolation between guest and hypervisor by removing vlan1 from the bridge itself.**  
On hypervisor side:
```
# bridge vlan del dev br0 vid 1 self
# bridge vlan show
    port            vlan ids
    eth1            1 PVID Egress Untagged
    tap0            1 PVID Egress Untagged
# tcpdump -n -v icmp
... no more packets received / sent ....
```

## LVBC behaviour
Using LVBC guarantees that the isolation between hypervisor and guests will be done correctly because LVBC automatically performs the following configurations:
- **enable vlan filtering and disabe iptables / ip6tables / arptables for the bridge**
    ```
    cat /sys/class/net/br0/bridge/vlan_filtering
    1
    cat /sys/class/net/br0/bridge/nf_call_iptables
    0
    cat /sys/class/net/br0/bridge/nf_call_ip6tables 
    0
    cat /sys/class/net/br0/bridge/nf_call_arptables 
    0
    ```
- **Use the vlan 1 only if user decide to used it in LVBC configuration**  

Concerning the last point, let's imagine that the vlan of the user network is vlan2.  
A classic manual configuration would give you the following dump with the associated risks mentioned above:
```
# bridge vlan show
        port            vlan ids
        eth1            1 PVID Egress Untagged
                        2 Egress Untagged

        br0             1 PVID Egress Untagged

        tap0            1 Egress Untagged
                        2 PVID Egress Untagged
```
 
Using such LVBC configuration json file:
```json
{"name": "br0",
"phys": "eth1",
"vlan1": false,
"stp": false,
"taps": [
    {"ifname": "tap0", "vlans": [
        {"vid": 2, "options":["pvid", "untagged"]}
        ]
    }
]
}
```
Would give you the following dump which ensure good isolation:
```
# bridge vlan show
        port            vlan ids
        eth1            1 PVID Egress Untagged
                        2 Egress Untagged

        tap0            2 PVID Egress Untagged
```
