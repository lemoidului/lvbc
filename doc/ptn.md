# Persistent taps interfaces naming

The bridge configuration used in LVBC requires that the various guest taps have a persistent name, especially in case of shutdown and restart the guests or while rebooting the hypervisor itself.  
Libvirt allows this through the configuration option "**target dev**" used in the domains XML files.  
Complete or add this option in your domain XML files (interface part) in order to obtain the following result:
```xml
<interface type='bridge'>
    <mac address='52:54:X:X:X:X'/>
    <source bridge='br0'/>
    <target dev='tap0-alpine1'/>
    <model type='virtio'/>
    <address type='pci' domain='0x0000' bus='0x00' slot='0x03' function='0x0'/>
</interface>
```
The use of the **virsh edit** command or the online XML domain edition  thanks to **virt-manager** GUI (diagram below) will allow you to fill in the "**target dev**" values required in your LVBC bridge configuration.  

![schema4](pics/schema4.png?raw=true "schema_4")