LVBC = $(CURDIR)/cmd/lvbc
LVBHOOK = $(CURDIR)/cmd/lvbhook
DIST = $(CURDIR)/dist
BRIDGES = $(CURDIR)/bridges

M = $(shell printf "\033[34;1m▶\033[0m")
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null)
DATE    ?= $(shell date +%FT%T)

LDFLAGS = -w -s
LDFLAGS:=$(LDFLAGS) -X lvbc/cmd.Version=$(VERSION) -X lvbc/cmd.BuildDate=$(DATE)
LDFLAGS:=$(LDFLAGS) -X lvbc/cmd.SocketPath=/run/lvbc.sock -X lvbc/cmd.Cpath=/usr/local/etc/lvbc
SOURCES := $(shell find $(CURDIR) -name "*.go")

$(LVBC)/lvbc: $(SOURCES)
	go build -ldflags '$(LDFLAGS)' -o $@ $(LVBC)/main.go
$(LVBHOOK)/lvbhook: $(SOURCES)
	go build -ldflags '$(LDFLAGS)' -o $@ $(LVBHOOK)/main.go

build: $(LVBC)/lvbc $(LVBHOOK)/lvbhook

OBJS=$(LVBC)/lvbc $(LVBHOOK)/lvbhook $(DIST)/lvbc.service $(DIST)/install.sh
$(DIST)/lvbc-$(VERSION).tar.gz: $(OBJS)
	$(eval TAR=$(notdir $@))
	@mkdir -p $(DIST)/lvbc-$(VERSION)
	@cp $(LVBC)/lvbc $(DIST)/lvbc-$(VERSION)/
	@cp $(LVBHOOK)/lvbhook $(DIST)/lvbc-$(VERSION)/
	@cp $(BRIDGES)/br0.json $(DIST)/lvbc-$(VERSION)/
	@cp $(DIST)/lvbc.service $(DIST)/lvbc-$(VERSION)/lvbc.service
	@cp $(DIST)/install.sh $(DIST)/lvbc-$(VERSION)/install.sh
	$(info $(M) create archive ..)
	@(cd $(DIST) ; tar -czf $(TAR) --remove-files lvbc-$(VERSION))

dist: $(DIST)/lvbc-$(VERSION).tar.gz

clean: ; $(info $(M) cleaning …)
	rm -f $(LVBC)/lvbc
	rm -f $(LVBHOOK)/lvbhook
	rm -f $(DIST)/lbvc-$(VERSION).tar.gz
	
version:
	@echo $(VERSION)

all: build

.PHONY: build all version clean dist

