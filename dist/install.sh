#!/bin/bash
set -e
[[ $(whoami) =~ "root" ]] || { echo "error, script must be run as root" ; exit 1; }
SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`
cd $SCRIPTPATH
echo "1/6: copying binaries files"
cp lvbc /usr/local/sbin/
cp lvbhook /etc/libvirt/hooks/
echo "2/6: linking qemu hooks"
[[ -f /etc/libvirt/hooks/qemu ]] && { mv /etc/libvirt/hooks/qemu /etc/libvirt/hooks/qemu_back; } 
ln -s /etc/libvirt/hooks/lvbhook /etc/libvirt/hooks/qemu
echo "3/6: copying conf sample"
mkdir -p /usr/local/etc/lvbc
cp br0.json /usr/local/etc/lvbc/br0.json.sample
echo "4/6: installing lvbc systemd unit"
cp lvbc.service /etc/systemd/system/
systemctl daemon-reload
echo "5/6: enable lvbc systemd unit"
systemctl enable lvbc.service
echo "6/6: lvbc installed, restart lvbc daemon"
