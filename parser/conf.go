package parser

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
)

var options = map[string]uint16{
	"master":      0,   // normally, master=0x1 but lib vishvananda/netlink
	"pvid":        0x2, // set master=0 for flag calculation.
	"untagged":    0x4,
	"range_begin": 0x8,
	"range_end":   0x10}

type Vlan struct {
	Vid      uint16   `json:"vid"`
	Options  []string `json:"options"`
	Flags    uint16
	Ancestor *Tap
}

type Tap struct {
	Ifname   string  `json:"ifname"`
	Vlans    []*Vlan `json:"vlans"`
	Ancestor *Bridge
}

type Bridge struct {
	Name     string `json:"name"`
	Phys     string `json:"phys"`
	Vlan1    bool   `json:"vlan1,omitempty"`
	Stp      bool   `json:"stp,omitempty"`
	Taps     []*Tap `json:"taps"`
	TapsMap  map[string]*Tap
	Ancestor *Config
}

type Config struct {
	Path       string
	Bridges    []*Bridge
	BridgesMap map[string]*Bridge
	TapsMap    map[string]*Tap
}

func (br *Bridge) parse(path string) (err error) {
	fd, err := os.Open(path)
	if err != nil {
		err_ := errors.New(fmt.Sprintf("Error reading config, file %s", err))
		log.Error(err_)
		return err_
	}
	defer fd.Close()
	// set default values for omitempty fields
	br.Stp = false
	br.Vlan1 = true
	decoder := json.NewDecoder(fd)
	err = decoder.Decode(&br)
	if err != nil {
		err_ := errors.New(fmt.Sprintf("Error parsing file %s, %s", path, err))
		log.Error(err_)
		return err
	}
	log.Debugf("bridge file %s, json ok", path)
	log.Debugln("bridge: ", br)
	br.TapsMap = make(map[string]*Tap)
	var pvid bool
	for _, tap := range br.Taps {
		vlans := make(map[uint16]struct{})
		// check for duplicate Tap ifname
		if _, ok := br.TapsMap[tap.Ifname]; ok {
			err = errors.New(
				fmt.Sprintf("%s->%s : tap defined twice", br.Name, tap.Ifname))
			log.Error(err)
			return err
		}
		br.TapsMap[tap.Ifname] = tap
		tap.Ancestor = br
		log.Debugln("tap: ", tap)

		pvid = false
		for _, vlan := range tap.Vlans {
			// check for duplicate vlans
			if _, ok := vlans[vlan.Vid]; ok {
				err = errors.New(
					fmt.Sprintf("%s->%s->vlan %d: vlan defined twice",
						br.Name, tap.Ifname, vlan.Vid))
				log.Error(err)
				return err
			}
			vlans[vlan.Vid] = struct{}{}
			// check for options && pvid, calculate corresponding flags
			for _, option := range vlan.Options {
				if flag, ok := options[option]; ok {
					vlan.Flags += flag
					if option == "pvid" {
						if pvid {
							err = errors.New(
								fmt.Sprintf("%s->%s->vlan %d: Pvid not uniq",
									br.Name, tap.Ifname, vlan.Vid))
							log.Error(err)
							return err
						} else {
							pvid = true
						}
					}
				} else {
					err = errors.New(
						fmt.Sprintf("%s->%s->vlan %d: option '%s' not allowed",
							br.Name, tap.Ifname, vlan.Vid, option))
					log.Error(err)
					return err
				}
			}
			log.Debugln("vlan flag", vlan.Flags)
			vlan.Ancestor = tap
			log.Debugln("vlan: ", vlan)
		}
	}
	return
}

func (cfg *Config) read() (err error) {
	files, err := ioutil.ReadDir(cfg.Path)
	if err != nil {
		return err
	}
	var bridge *Bridge
	cfg.BridgesMap = make(map[string]*Bridge)
	cfg.TapsMap = make(map[string]*Tap)
	br_phys := make(map[string]struct{})
	for _, file := range files {
		// file extension check
		filext := filepath.Ext(file.Name())
		if filext != ".json" {
			log.Warningf("file %s not a json extension, will be ignore",
				file.Name())
			continue
		}
		filep := filepath.Join(cfg.Path, file.Name())
		log.Infoln("parsing bridge file:", filep)
		bridge = new(Bridge)
		err = bridge.parse(filep)
		if err != nil {
			return err
		}
		// check for duplicate bridge name
		if _, ok := cfg.BridgesMap[bridge.Name]; ok {
			err = errors.New(
				fmt.Sprintf("bridge %s defined twice", bridge.Name))
			log.Error(err)
			return err
		}
		cfg.BridgesMap[bridge.Name] = bridge
		// check for duplicate physical interface
		if _, ok := br_phys[bridge.Phys]; ok {
			err = errors.New(
				fmt.Sprintf("bridge %s, interface %s already used in other bridge",
					bridge.Name, bridge.Phys))
			log.Error(err)
			return err
		}
		br_phys[bridge.Phys] = struct{}{}
		// check for duplicate taps
		for k, tap := range bridge.TapsMap {
			if _, ok := cfg.TapsMap[k]; ok {
				err = errors.New(
					fmt.Sprintf("bridge %s, interface %s already used in other bridge",
						bridge.Name, k))
				log.Error(err)
				return err
			}
			cfg.TapsMap[k] = tap
		}
		cfg.Bridges = append(cfg.Bridges, bridge)
	}
	return err
}

func NewConfig(path string) (*Config, error) {
	var err error
	var conf *Config
	conf = new(Config)
	conf.Path = path
	err = conf.read()
	return conf, err
}
