package parser

import (
	"encoding/xml"
	"errors"
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"
)

func DomainParse(domxml string) ([]string, error) {
	// whatever language, xml suck !
	type Source struct {
		Bridge string `xml:"bridge,attr"`
	}
	type Target struct {
		Dev string `xml:"dev,attr"`
	}
	type Interface struct {
		XMLName xml.Name `xml:"interface"`
		Type    string   `xml:"type,attr"`
		Source  Source   `xml:"source"`
		Target  Target   `xml:"target"`
	}
	type Domain struct {
		XMLName    xml.Name    `xml:"domain"`
		Type       string      `xml:"type,attr"`
		Name       string      `xml:"name"`
		Interfaces []Interface `xml:"devices>interface"`
	}
	r := strings.NewReader(domxml)
	decoder := xml.NewDecoder(r)
	var domain Domain
	err := decoder.Decode(&domain)
	if err != nil {
		err_ := errors.New(fmt.Sprintln("DomainParse error: ", err))
		log.Error(err_)
		return nil, err
	}
	log.Debugln("DomainParse:", domain)

	taps := []string{}
	if domain.Type != "kvm" {
		log.Warnf("domain %s, not kvm eligible", domain.Name)
		return nil, nil
	}
	for _, dom_if := range domain.Interfaces {
		if dom_if.Type != "bridge" {
			log.Warnf("domain %s, tap interface not bridge type -> skipping",
				domain.Name)
			continue
		}
		target := dom_if.Target.Dev
		if target == "" {
			log.Warnf("domain %s, no target found for tap -> skipping",
				domain.Name)
			continue
		}
		bridge := dom_if.Source.Bridge
		if bridge == "" {
			log.Warnf("domain %s, no bridge found for tap %s-> skipping",
				domain.Name, target)
			continue
		}
		taps = append(taps, dom_if.Target.Dev)

	}
	if len(taps) == 0 {
		log.Warnf("domain %s, no eligible taps found -> skipping", domain.Name)
		return nil, nil
	}
	return taps, nil
}
