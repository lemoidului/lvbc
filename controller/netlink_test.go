package controller

import (
	"fmt"
	"lvbc/parser"
	"testing"

	log "github.com/sirupsen/logrus"
)

// sudo -E env "PATH=$PATH" go test '-run=^Test_BridgeCheck$'
func Test_BridgeCheck(t *testing.T) {
	bridge := &parser.Bridge{Name: "br0", Phys: "enp0s25", Vlan1: true, Stp: false}
	fmt.Println(BridgeCheck(bridge, false))
}

// sudo -E env "PATH=$PATH" go test '-run=^Test_TapCheck$'
func Test_TapCheck(t *testing.T) {
	log.SetLevel(log.DebugLevel)
	bridge := &parser.Bridge{Name: "br0", Phys: "enp0s25", Vlan1: true, Stp: false}
	tap := &parser.Tap{Ifname: "tap0-alpine1", Ancestor: bridge}
	vlan1 := &parser.Vlan{Vid: 1, Flags: 6, Ancestor: tap}
	vlan2 := &parser.Vlan{Vid: 2, Flags: 4, Ancestor: tap}
	tap.Vlans = []*parser.Vlan{vlan1, vlan2}
	fmt.Println(TapCheck(tap, false))
}

// sudo -E env "PATH=$PATH" go test '-run=^Test_PhysVdel$'
func Test_PhysVdel(t *testing.T) {
	log.SetLevel(log.DebugLevel)
	bridge := &parser.Bridge{Name: "br0", Phys: "enp0s25", Vlan1: true, Stp: false}
	fmt.Println(PhysVdel(bridge, false))
}
