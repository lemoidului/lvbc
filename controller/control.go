package controller

import (
	"lvbc/parser"

	log "github.com/sirupsen/logrus"
)

func ConfApply(config *parser.Config) (err error) {
	dry_run := [2]bool{true, false}
	for _, dr := range dry_run {
		switch dr {
		case true:
			log.Info("---- Apply Configuration Dry Run ----")
		case false:
			log.Info("-------- Apply Configuration --------")
		}
		for _, bridge := range config.BridgesMap {
			err = BridgeCheck(bridge, dr)
			if err != nil {
				return err
			}
		}
		for _, tap := range config.TapsMap {
			err = TapCheck(tap, dr)
			if err != nil {
				return err
			}
		}
		for _, bridge := range config.BridgesMap {
			err = PhysVdel(bridge, dr)
			if err != nil {
				return err
			}
		}
	}
	return
}

func TapsAdd(domtaps []string, config *parser.Config) (err error) {

	if len(domtaps) == 0 {
		log.Warn("qemu domain, nothing to configure")
		return
	}
	taps := []*parser.Tap{}
	for _, domtap := range domtaps {
		if tapval, ok := config.TapsMap[domtap]; ok {
			taps = append(taps, tapval)
		}
	}
	dry_run := [2]bool{true, false}
	for _, dr := range dry_run {
		switch dr {
		case true:
			log.Info("---- Apply Tap Config Dry Run ----")
		case false:
			log.Info("------- Apply Tap Config ---------")
		}
		for _, tap := range taps {
			err = TapCheck(tap, dr)
			if err != nil {
				return err
			}
		}
	}
	return
}

func TapsDel(domtaps []string, config *parser.Config) (err error) {

	if len(domtaps) == 0 {
		log.Warn("qemu domain, nothing to configure")
		return
	}
	mbridges := make(map[string]*parser.Bridge)
	for _, domtap := range domtaps {
		if tap, ok := config.TapsMap[domtap]; ok {
			if _, okb := mbridges[tap.Ancestor.Name]; !okb {
				mbridges[tap.Ancestor.Name] = tap.Ancestor
			}
		}
	}
	dry_run := [2]bool{true, false}
	for _, dr := range dry_run {
		switch dr {
		case true:
			log.Info("---- Delete Vlans Dry Run ----")
		case false:
			log.Info("------- Delete Vlans ---------")
		}
		for _, bridge := range mbridges {
			err = PhysVdel(bridge, dr)
			if err != nil {
				return err
			}
		}
	}
	return
}
