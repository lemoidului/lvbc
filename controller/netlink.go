package controller

import (
	"errors"
	"fmt"
	"io/ioutil"
	"lvbc/parser"

	log "github.com/sirupsen/logrus"
	"github.com/vishvananda/netlink"
	"github.com/vishvananda/netlink/nl"
)

// brSys write given <value> into /sys/class/net/<bridge>/<arg>
// if dr (dry run) si true, value is not written (log only)
// brSys provide netlink lib missing functionalities
func brSys(brname string, arg string, val string, dr bool) error {
	path := fmt.Sprintf("/sys/class/net/%s/bridge/%s", brname, arg)
	bread, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}
	// remove EOL
	bread = bread[:len(bread)-1]
	if string(bread) != val {
		log.Infof("writing '%s' in %s", val, path)
		if !dr {
			err = ioutil.WriteFile(path, []byte(val), 0)
		}
	}
	return err
}

// brAdd create and configure a new bridge
func brAdd(br *parser.Bridge) error {
	bra := netlink.NewLinkAttrs()
	bra.Name = br.Name
	vlanfiltering := true
	bridge := &netlink.Bridge{LinkAttrs: bra, VlanFiltering: &vlanfiltering}
	err := netlink.LinkAdd(bridge)
	if err != nil {
		err_ := errors.New(fmt.Sprintf("could not add %s: %v", br.Name, err))
		return err_
	}
	// by default linux bridge add vlan 1 (pvid, untagged)
	if !br.Vlan1 {
		err = netlink.BridgeVlanDel(bridge, 1, true, true, true, false)
		if err != nil {
			log.Error(err)
			return err
		}
	}
	// by default linux bridge set stp_state to 0
	if br.Stp {
		err = brSys(br.Name, "stp_state", "1", false)
		if err != nil {
			log.Error(err)
			return err
		}
	}
	linkp, _ := netlink.LinkByName(br.Phys)
	err = netlink.LinkSetMaster(linkp, bridge)
	if err != nil {
		err_ := errors.New(fmt.Sprintf("could not add %s to bridge %s: %v",
			br.Phys, br.Name, err))
		return err_
	}
	err = netlink.LinkSetUp(bridge)
	return err
}

// BridgeCheck check and apply bridge configuration.
// If dr (dry run) is true, only check configuration is done.
func BridgeCheck(br *parser.Bridge, dr bool) error {
	// first check if bridge interface exists (as a netlink.link)
	notExists := false
	brlink, err := netlink.LinkByName(br.Name)
	if err != nil {
		if errors.As(err, &netlink.LinkNotFoundError{}) {
			notExists = true
		} else {
			log.Error(err)
			return err
		}
	}
	// check bridge slave interface
	linkp, err := netlink.LinkByName(br.Phys)
	if err != nil {
		if errors.As(err, &netlink.LinkNotFoundError{}) {
			err = errors.New(
				fmt.Sprintf("interface %s member of bridge %s not exists",
					br.Phys, br.Name))
		}
		log.Error(err)
		return err
	}
	// physical interfaces must not be configured with ip addr
	addrl, err := netlink.AddrList(linkp, netlink.FAMILY_V4)
	if err != nil {
		log.Error(err)
		return err
	}
	if len(addrl) > 0 {
		err = errors.New(
			fmt.Sprintf("interface %s is in use, could not belongs to bridge %s",
				br.Phys, br.Name))
		log.Error(err)
		return err
	}
	// physical interface must be up
	if linkp.Attrs().OperState != netlink.OperUp {
		log.Infof("set interface %s up", br.Phys)
		if !dr {
			err = netlink.LinkSetUp(linkp)
			if err != nil {
				log.Error(err)
				return err
			}
		}
	}
	// check physical interface master
	linkp_master := linkp.Attrs().MasterIndex
	if notExists {
		if linkp_master != 0 {
			link_master, _ := netlink.LinkByIndex(linkp_master)
			err = errors.New(fmt.Sprintf("interface %s belongs to bridge %s",
				br.Phys, link_master.Attrs().Name))
			log.Error(err)
			return err
		}
		log.Infof("create %s bridge", br.Name)
		if !dr {
			err = brAdd(br)
			if err != nil {
				log.Error(err)
				return err
			}
		}
	} else {
		// check if brlink is a bridge
		bridge, ok := brlink.(*netlink.Bridge)
		if !ok {
			err = errors.New(fmt.Sprintf("interface %s is not a bridge", br.Name))
			log.Error(err)
			return err
		}
		// check physical interface master
		switch {
		case linkp_master == 0:
			log.Infof("attach %s to bridge %s", br.Phys, br.Name)
			if !dr {
				err = netlink.LinkSetMaster(linkp, bridge)
				if err != nil {
					log.Error(err)
					return err
				}
			}
		case linkp_master != bridge.Attrs().Index:
			link_master, _ := netlink.LinkByIndex(linkp_master)
			err = errors.New(fmt.Sprintf("interface %s belongs to bridge %s",
				br.Phys, link_master.Attrs().Name))
			log.Error(err)
			return err
		}
		// check vlan_filtering
		if !*bridge.VlanFiltering {
			err = brSys(br.Name, "vlan_filtering", "1", dr)
			if err != nil {
				log.Error(err)
				return err
			}
		}
		// check vlan1
		vlanlist, err := netlink.BridgeVlanList()
		if err != nil {
			log.Error(err)
			return err
		}
		vid1 := false
		for _, vinfo := range vlanlist[int32(bridge.Attrs().Index)] {
			if vinfo.Vid == 1 {
				vid1 = true
				break
			}
		}
		if vid1 != br.Vlan1 {
			if br.Vlan1 {
				log.Infof("adding vlan1 to bridge %s", br.Name)
				if !dr {
					err = netlink.BridgeVlanAdd(bridge, 1, true, true, true, false)
				}
			} else {
				log.Infof("remove vlan1 to bridge %s", br.Name)
				if !dr {
					err = netlink.BridgeVlanDel(bridge, 1, true, true, true, false)
				}
			}
			if err != nil {
				log.Error(err)
				return err
			}
		}
		// check stp
		stp_val := "0"
		if br.Stp {
			stp_val = "1"
		}
		err = brSys(br.Name, "stp_state", stp_val, dr)
		if err != nil {
			log.Error(err)
			return err
		}
		// check if bridge is up
		if bridge.Attrs().OperState != netlink.OperUp {
			log.Infof("set bridge %s up", br.Name)
			if !dr {
				err = netlink.LinkSetUp(bridge)
				if err != nil {
					log.Error(err)
					return err
				}
			}
		}
	}
	return nil
}

// vlanMasterAdd add a new a vlan entry.
// Do nothing if dr (dry run) is true
func vlanMasterAdd(link netlink.Link, vinfo *nl.BridgeVlanInfo, dr bool) error {
	if dr {
		return nil
	}
	return netlink.BridgeVlanAdd(link, vinfo.Vid, vinfo.PortVID(), vinfo.EngressUntag(),
		false, true)
}

// vlanMasterDel del a vlan entry.
// Do nothing if dr (dry run) is true.
func vlanMasterDel(link netlink.Link, vinfo *nl.BridgeVlanInfo, dr bool) error {
	if dr {
		return nil
	}
	return netlink.BridgeVlanDel(link, vinfo.Vid, vinfo.PortVID(), vinfo.EngressUntag(),
		false, true)
}

// TapCheck check and apply vlan configuration against a given tap
// If dr (dry run) is true, only check configuration is done.
func TapCheck(tap *parser.Tap, dr bool) error {
	// check tap link
	tlink, err := netlink.LinkByName(tap.Ifname)
	if err != nil {
		if errors.As(err, &netlink.LinkNotFoundError{}) {
			log.Infof("tap interface %s not found", tap.Ifname)
			return nil
		} else {
			log.Error(err)
			return err
		}
	}
	// check master
	midx := tlink.Attrs().MasterIndex
	if midx == 0 {
		err := errors.New(fmt.Sprintf("tap %s not belongs to any bridge",
			tap.Ifname))
		log.Error(err)
		return err
	}
	mlink, err := netlink.LinkByIndex(midx)
	if err != nil {
		log.Error(err)
		return err
	}
	if mlink.Attrs().Name != tap.Ancestor.Name {
		err := errors.New(fmt.Sprintf("tap %s slaveof %s and not %s", tap.Ifname,
			mlink.Attrs().Name, tap.Ancestor.Name))
		log.Error(err)
		return err
	}
	// check vlans
	vlanlist, err := netlink.BridgeVlanList()
	if err != nil {
		log.Error(err)
		return err
	}
	plink, err := netlink.LinkByName(tap.Ancestor.Phys)
	if err != nil {
		log.Error(err)
		return err
	}
	tapvlans := make(map[uint16]*nl.BridgeVlanInfo)
	physvlans := make(map[uint16]*nl.BridgeVlanInfo)
	confvlans := make(map[uint16]*nl.BridgeVlanInfo)
	for _, info := range vlanlist[int32(tlink.Attrs().Index)] {
		tapvlans[info.Vid] = info
	}
	for _, info := range vlanlist[int32(plink.Attrs().Index)] {
		physvlans[info.Vid] = info
	}
	for _, vlan := range tap.Vlans {
		confvlans[vlan.Vid] = &nl.BridgeVlanInfo{Vid: vlan.Vid, Flags: vlan.Flags}
		if tvlan, ok := tapvlans[vlan.Vid]; ok {
			if tvlan.Flags != confvlans[vlan.Vid].Flags {
				log.Infof("reconfigure vlan %d options for tap %s",
					vlan.Vid, tap.Ifname)
				err = vlanMasterAdd(tlink, confvlans[vlan.Vid], dr)
				if err != nil {
					log.Error(err)
					return err
				}
			}
		} else {
			log.Infof("adding vlan %d for tap %s", vlan.Vid, tap.Ifname)
			err = vlanMasterAdd(tlink, confvlans[vlan.Vid], dr)
			if err != nil {
				log.Error(err)
				return err
			}
		}
		if _, ok := physvlans[vlan.Vid]; !ok {
			log.Infof("adding vlan %d on interface %s",
				vlan.Vid, plink.Attrs().Name)
			err = vlanMasterAdd(plink, &nl.BridgeVlanInfo{Vid: vlan.Vid, Flags: 0}, dr)
			if err != nil {
				log.Error(err)
				return err
			}
		}
	}
	for vid, vinfo := range tapvlans {
		if _, ok := confvlans[vid]; !ok {
			log.Infof("delete vlan %d from tap %s", vid, tlink.Attrs().Name)
			err = vlanMasterDel(tlink, vinfo, dr)
			if err != nil {
				log.Error(err)
				return err
			}
		}
	}
	return nil
}

// PhysVdel delete vlan(s) on bridge physical interface
// If dr (dry run) is true, only check is done.
func PhysVdel(bridge *parser.Bridge, dr bool) (err error) {

	plink, err := netlink.LinkByName(bridge.Phys)
	if err != nil {
		if errors.As(err, &netlink.LinkNotFoundError{}) {
			return
		} else {
			log.Error(err)
			return
		}
	}
	pidx := int32(plink.Attrs().Index)
	vlans, err := netlink.BridgeVlanList()
	if err != nil {
		log.Error(err)
		return
	}
	pvinfos := vlans[pidx]
	// keep every keys in vlans map except the physical interface (plink)
	delete(vlans, pidx)
	// usevids represents vlans still in use by all interfaces except phys
	usevids := make(map[uint16]struct{})
	for _, infos := range vlans {
		for _, info := range infos {
			usevids[info.Vid] = struct{}{}
		}
	}
	for _, pvinfo := range pvinfos {
		if pvinfo.Vid == 1 {
			continue
		}
		if _, ok := usevids[pvinfo.Vid]; !ok {
			log.Infof("delete vlan %d for interface %s", pvinfo.Vid, bridge.Phys)
			err = vlanMasterDel(plink, pvinfo, dr)
			if err != nil {
				err = errors.New(fmt.Sprintf("errors deleting vlan %d for interface %s: %v",
					pvinfo.Vid, bridge.Phys, err))
				log.Error(err)
				return
			}
		}
	}
	return
}
