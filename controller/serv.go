package controller

import (
	"bytes"
	"context"
	"encoding/binary"
	"lvbc/cmd"
	"lvbc/ipc"
	"lvbc/parser"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/coreos/go-systemd/daemon"
	log "github.com/sirupsen/logrus"
)

func msgQueue(ctx context.Context, msgs chan *ipc.Message, conf *parser.Config) {
	config := conf
	cvalid := true
	capply := true
	for {
		select {
		case msg := <-msgs:
			log.Debug("Processing new message: ", msg.Type)
			switch msg.Type {
			case ipc.RCONF:
				log.Info("lvbc SIGHUP, reload configuration")
				var err error
				config, err = parser.NewConfig(config.Path)
				if err != nil {
					log.Error("error while reading new configuration")
					log.Error("daemon will not apply configuration")
					cvalid = false
				} else {
					log.Info("New configuration reading success")
					cvalid = true
					err = ConfApply(config)
					if err != nil {
						log.Error("error while apply new configuration")
						log.Error("daemon will not apply configuration")
						capply = false
					} else {
						capply = true
						log.Info("lvbc, reload configuration succes")
					}
				}
			default:
				switch {
				case !cvalid:
					log.Errorf("configuration %s error, lvbc disabled", config.Path)
				case !capply:
					log.Errorf("configuration %s can not be apply, lvbc disabled", config.Path)
				default:
					switch msg.Type {
					case ipc.QSTART:
						domtaps, err := parser.DomainParse(msg.Content)
						if err == nil {
							TapsAdd(domtaps, config)
						}
					case ipc.QRELEASE:
						domtaps, err := parser.DomainParse(msg.Content)
						if err == nil {
							TapsDel(domtaps, config)
						}
					}
				}
			}
		case <-ctx.Done():
			log.Debug("msgQueue quit")
			return
		}
	}
}

func Serv(conf *parser.Config) error {
	err := os.Remove(cmd.SocketPath)
	if err != nil && !os.IsNotExist(err) {
		log.Error("ipc server error:", err)
		return err
	}
	socket, err := net.Listen("unix", cmd.SocketPath)
	if err != nil {
		log.Error("ipc server error: ", err)
		return err
	}
	defer socket.Close()
	log.Info("ipc server Start")
	// systemd daemon notify
	_, err = daemon.SdNotify(false, daemon.SdNotifyReady)
	if err != nil {
		log.Error("systemd notify error:", err)
	}
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGTERM, syscall.SIGINT, syscall.SIGHUP)
	conns := make(chan net.Conn, 10)
	msgs := make(chan *ipc.Message, 10)
	ctx, cancel := context.WithCancel(context.Background())
	go msgQueue(ctx, msgs, conf)
	go func() {
		for {
			conn, err := socket.Accept()
			if err != nil {
				log.Debug("ipc server sock accept errror: ", err)
				return
			}
			log.Info("ipc server, new connection")
			conns <- conn
		}
	}()
	for {
		select {
		case sig := <-sigchan:
			switch sig {
			case syscall.SIGHUP:
				msg := &ipc.Message{ipc.RCONF, ""}
				msgs <- msg
			default:
				log.Warn("interrupt signal, ipc server will stop")
				cancel()
				return err
			}
		case conn := <-conns:
			go func(conn net.Conn) {
				defer conn.Close()
				log.Debug("ipc connection, awaiting msg")
				conn.SetDeadline(time.Now().Add(5 * time.Second))
				head := make([]byte, 2)
				var dsize uint16
				_, err := conn.Read(head)
				if err != nil {
					log.Warn("ipc connection, error read socket: ", err)
					return
				}
				rb := bytes.NewReader(head)
				err = binary.Read(rb, binary.LittleEndian, &dsize)
				data := make([]byte, dsize)
				copy(data[:2], head)
				_, err = conn.Read(data[2:])
				if err != nil {
					log.Warn("ipc connection, error read socket: ", err)
					return
				}
				msg, err := ipc.NewMessage(data)
				if err != nil {
					log.Warn("ipc connection, error decoding message: ", err)
					return
				}
				log.Info("ipc server, new message: ", msg.Type)
				// ipc LSTART must not release conn immediately
				if msg.Type == ipc.LSTART {
					//long task simulation
					time.Sleep(4 * time.Second)
					conn.Write([]byte{0x00})
				} else {
					// others ipc messages release conn instantly
					// msg are push in queue before processing
					msgs <- msg
					conn.Write([]byte{0x00})
				}
			}(conn)
		}
	}
}
