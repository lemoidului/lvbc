package cmd

// dynamic go build vars (ldflags -X)
var (
	Version    = "dev"
	BuildDate  = "unknown"
	SocketPath = "/tmp/lvbc.sock"
	Cpath      = "./bridges"
)

const (
	Repo   = "gitlab.com/lemoidului/lvbc"
	Banner = `
	 ████              █████             
	░░███             ░░███              
 	 ░███  █████ █████ ░███████   ██████ 
 	 ░███ ░░███ ░░███  ░███░░███ ███░░███
 	 ░███  ░███  ░███  ░███ ░███░███ ░░░ 
 	 ░███  ░░███ ███   ░███ ░███░███  ███
 	 █████  ░░█████    ████████ ░░██████ 
	░░░░░    ░░░░░    ░░░░░░░░   ░░░░░░  
                                                
	`
)
