package main

import (
	"flag"
	"fmt"
	"lvbc/cmd"
	"lvbc/controller"
	"lvbc/parser"
	"os"
	"runtime"

	log "github.com/sirupsen/logrus"
)

func main() {
	fmt.Println(cmd.Banner)
	fmt.Printf("Repo        : %s\n", cmd.Repo)
	fmt.Printf("Version     : %s\n", cmd.Version)
	fmt.Printf("Build       : %s\n", cmd.BuildDate)
	fmt.Printf("Go Ver      : %s\n\n", runtime.Version())

	debug := flag.Bool("d", false, "log debug mode")
	confp := flag.String("c", cmd.Cpath, "bridges config directory")
	flag.Parse()

	// LSYSD env is set in lvbc systemd unit
	if _, lsysd := os.LookupEnv("LSYSD"); lsysd {
		log.SetFormatter(&log.TextFormatter{DisableTimestamp: true})
	} else {
		log.SetFormatter(&log.TextFormatter{FullTimestamp: true})
	}
	if *debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}
	log.Info("try reading bridges directory: ", *confp)
	conf, err := parser.NewConfig(*confp)
	if err != nil {
		log.Fatal("bad configuration, daemon will not start")
	}
	log.Info("parsing configuration success")
	err = controller.ConfApply(conf)
	if err != nil {
		log.Fatal("configuration will not be apply, daemon will not start")
	}
	log.Info("configuration apply success, daemon will start")
	controller.Serv(conf)
}
