package main

import (
	"bufio"
	"errors"
	"log/syslog"
	"lvbc/cmd"
	"lvbc/ipc"
	"net"
	"os"
	"path"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	lSyslog "github.com/sirupsen/logrus/hooks/syslog"
)

var log = logrus.New()

func send_msg(msg *ipc.Message) (err error) {
	data, err := msg.Pack()
	// log.Debugf("msg data: %#v", data)
	if err != nil {
		log.Error(err)
		return
	}
	conn, err := net.Dial("unix", cmd.SocketPath)
	if err != nil {
		log.Error("connection error: ", err)
		return
	}
	defer conn.Close()
	log.Info("send message: ", msg.Type)
	conn.SetDeadline(time.Now().Add(5 * time.Second))
	_, err = conn.Write(data)
	if err != nil {
		log.Error("connection write error: ", err)
		return
	}
	buf := make([]byte, 1)
	_, err = conn.Read(buf)
	if err != nil {
		log.Error("connection reading error: ", err)
		return
	}
	log.Info("hook message sent")
	return
}

func stdin() (*string, error) {
	// return the stdin *string with a 1s timeout
	// implement a non-blocking Multi Scanln function

	ch := make(chan string)
	cherr := make(chan error)

	go func() {
		scanner := bufio.NewScanner(os.Stdin)
		lines := []string{}
		for {
			retscan := scanner.Scan()
			if !retscan {
				break
			}
			line := scanner.Text()
			lines = append(lines, line)
		}
		xml := strings.Join(lines, "\n")
		err := scanner.Err()
		if err != nil {
			cherr <- err
		} else {
			ch <- xml
		}
	}()
	select {
	case err := <-cherr:
		return nil, err
	case xml := <-ch:
		return &xml, nil
	case <-time.After(1 * time.Second):
		err := errors.New("timeout")
		return nil, err
	}
}

func main() {
	// busybox style: specific links target lvbhook, thus
	// analysing argv[0] give behaviour.
	// libvirt hook: https://libvirt.org/hooks.html

	// Configure log for syslog
	hook, err := lSyslog.NewSyslogHook("", "", syslog.LOG_INFO, "lvbhook")
	if err == nil {
		log.Hooks.Add(hook)
	}
	caller := path.Base(os.Args[0])
	switch caller {
	case "qemu":
		if len(os.Args) > 2 {
			xml, err := stdin()
			if err != nil {
				log.Error("xml stdin error: ", err)
				return
			}
			switch os.Args[2] {
			// start occurs after libvirt has finished labeling all resources
			// but has not yet started the guest
			// etc/libvirt/hooks/qemu guest_name start begin -
			case "start":
				msg := &ipc.Message{ipc.QSTART, *xml}
				log.Infof("hook qemu domain %s start", os.Args[1])
				log.Debug("xml: ", *xml)
				send_msg(msg)
			// release call after libvirt has released all domain resources
			// etc/libvirt/hooks/qemu guest_name release end -
			case "release":
				msg := &ipc.Message{ipc.QRELEASE, *xml}
				log.Infof("hook qemu domain %s release", os.Args[1])
				log.Debug("xml: ", *xml)
				send_msg(msg)
			default:
				log.Debug("qemu hook ignoring: ", os.Args[2])
				return
			}
		} else {
			log.Info("qemu hook ignoring")
		}
	case "daemon":
		if len(os.Args) > 2 {
			switch os.Args[2] {
			case "start":
				// etc/libvirt/hooks/daemon - start - start
				msg := &ipc.Message{ipc.LSTART, ""}
				log.Info("hook daemon libvirt start")
				send_msg(msg)
			case "shutdown":
				// etc/libvirt/hooks/daemon - shutdown - shutdown
				msg := &ipc.Message{ipc.LSTOP, ""}
				log.Info("hook daemon libvirt stop")
				send_msg(msg)
			default:
				log.Debug("hook daemon libvirt ignoring: ", os.Args[2])
				return
			}
		} else {
			log.Info("daemon hook ignoring")
		}
	default:
		log.Info("ignoring call: ", caller)
		log.Debug("ignoring hook: ", os.Args)
	}
}
