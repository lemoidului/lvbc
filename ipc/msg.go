package ipc

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"

	log "github.com/sirupsen/logrus"
)

type Message struct {
	Type    MsgType
	Content string
}

type MsgType byte

const (
	UNDEF MsgType = iota
	RCONF
	LSTART
	LSTOP
	QSTART
	QRELEASE
)

func (msg MsgType) String() string {
	switch msg {
	case RCONF:
		return "rconf"
	case LSTART:
		return "lstart"
	case LSTOP:
		return "lstop"
	case QSTART:
		return "qstart"
	case QRELEASE:
		return "qrelease"
	}
	return "undef"
}

func (msg *Message) Pack() ([]byte, error) {
	// Create data []bytes serialization of given Message
	// Serialization data schema:
	// Header=data_size (2 bytes) | Msg_Type ( 1 byte) | Msg_Content
	var err error
	bcontent := []byte(msg.Content)
	lbcontent := len(bcontent)
	if lbcontent > 0xffff-3 {
		err := errors.New(fmt.Sprintf("Message content size too large: %d",
			lbcontent))
		log.Error(err)
		return nil, err
	}
	// first create data slice (length = capacity = lbcontent+3)
	data := make([]byte, lbcontent+3)
	// set header length
	head := make([]byte, 2)
	binary.LittleEndian.PutUint16(head, uint16(lbcontent+3))
	copy(data[0:2], head)
	// then, set Message type
	data[2] = byte(msg.Type)
	// finally set msg_content
	copy(data[3:], bcontent)
	return data, err
}

func (msg *Message) unPack(data []byte) error {
	// Unserialize data []bytes into given *Message values
	var err error
	ldata := len(data)
	if ldata < 3 {
		err := errors.New("Bad data size")
		log.Error(err)
		return err
	}
	// Check data size against header
	var head_size uint16
	head := bytes.NewReader(data[0:2])
	err = binary.Read(head, binary.LittleEndian, &head_size)
	if err != nil {
		log.Error(err)
		return err
	}
	if int(head_size) != ldata {
		err = errors.New("Bad data integrity")
		log.Error(err)
		return err
	}
	msg.Type = MsgType(data[2])
	msg.Content = string(data[3:])
	return err
}

func NewMessage(data []byte) (*Message, error) {
	// return a new *Message for a provided []bytes serialization data
	msg := &Message{}
	err := msg.unPack(data)
	return msg, err
}
