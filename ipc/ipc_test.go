package ipc

import (
	"bytes"
	"fmt"
	"testing"
)

func Test(t *testing.T) {
	var err error
	msg := &Message{LSTART, "ugly xml"}
	fmt.Println("msg:", *msg)
	pack, err := msg.Pack()
	fmt.Printf("pack: %#v\n", pack)
	msg2, err := NewMessage(pack)
	fmt.Println("msg2", *msg2)
	pack2, err := msg2.Pack()
	fmt.Printf("pack2 %#v\n", pack2)
	if err != nil {
		fmt.Println("Test error", err)
	}
	if res := bytes.Compare(pack, pack2); res != 0 {
		t.Fatal("pack != pack2")
	}
}
