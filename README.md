# LVBC - Libvirt Vlan Bridge Configurator

## Description
LVBC is a daemon for configuring a linux bridge that supports vlan filtering and maintains persistent vlan configuration states for kvm / qemu hypervisor based virtual machines.  
LVBC is not dedicated to a specific network manager and can run on any linux systems that supports the kvm/qemu/libvirt suite.  
LVBC cannot work without the libvirt packages and especially the libvirtd daemon.  
LVBC is written in Go and released under the GPL license.  

![schema](doc/pics/schema1.png?raw=true "schema_1")


## Using LVBC ?
- LVBC corresponds to the very common needs in production to serve guests on differents vlans while there is not yet an integrated solution in libvirt to manage this use case.
- You are concerned about ensuring effective isolation between an administrative network and guests machines or/and between each guests.
- For people who prefer simple and customizable solutions for their kvm hypervisor (virsh , virt-manager over ssh) compared to ready-made solutions like [Proxmox](https://proxmox.com/en/).
- Because managing persistent bridge and network interfaces configuration in Linux distributions become a really mess nowadays with all existing and increasing disparities in userland tools (i'm not talking about iproute2 which is a powerful and well-made utility but unfortunately does not allow persistence).
- Vlan filtering capabilities offered by the linux kernel bridge and use by LVBC avoid deploying more complex solution like [Open vSwitch](https://www.openvswitch.org/) when your goal is only to manage vlans efficiently.

## Installation and Usage

The easiest way for installing LVBC is to use the latest [releases package](https://gitlab.com/lemoidului/lvbc/-/releases).  
Once package downloaded: 
```bash
tar -xvzf lvbc-<version>.tar.gz
cd lvbc-<version>
sudo ./install.sh

# Edit bridge json files in /usr/local/etc/lvbc
# (a sample json file is given in /usr/local/etc/lvbc/br0.json.sample)
# It's possible to provide multiple configuration files for multiple bridges
# Once configuration done, launch lvbc daemon
sudo systemctl start lvbc

# relaunch libvirtd daemon if apparmor is in used (on Debian base distros)
sudo systemctl restart libvirtd

# Check daemons and configuration
sudo journalctl -u libvirtd
sudo journalctl -u lvbc
sudo bridge vlan show
```
LVBC systemd unit is set enable during install process (active for next reboot)  and bridge configuration will be automatically apply when kvm guests domains are start / destroy.  
In case of reboot, LVBC daemon will be start before libvirtd. Configuration will be immediatly apply for all kvm guests marked as autostart.  

### Change and apply configuration
```bash
# Edit bridge(s) configuration in /usr/local/etc/lvbc
# Once done, reload LVBC configuration with
sudo systemctl reload lvbc
```

### Error in bridges configuration files 
- **Error during initial start**   
If error(s) are detected during intial configuration parsing, LVBC will display error(s) origin in journalctl and will refuse to start.
- **Error during reload**  
If error(s) are detected during reload configuration, LVBC will display error(s) origin in journalctl and will be set in a downgraded mode.
Automatic bridge configuration will be disabled for all guests that could be started or stopped later.  
Normal behavior of LVBC will be reached again when an acceptable configuration will be edited and reloaded.  

In all cases, priority is given to **libvirtd** which will always be able to start or shutdown machines whatever the state of the LVBC daemon.  
If LVBC is not started or is in a downgraded mode, the automatic bridges configuration can not be achieved. At same times, error(s) warning messages are always provided in the LVBC log files.

### Customize LVBC configuration files location
LVBC uses the various bridge configuration JSON files in the following default path: "**/usr/local/etc/lvbc**".  
It's possible to customize this value with a startup option specified in the systemd unit. To do this, edit the LVBC unit as follows:  
```bash
# First stop LVBC daemon
sudo systemctl stop lvbc
# Then, edit /etc/systemd/system/lvbc.service 
...
[Service]
Type=notify
# ExecStart=/usr/local/sbin/lvbc
ExecStart=/usr/local/sbin/lvbc -c <your_full_path_here>
ExecReload=/bin/kill -HUP $MAINPID
...
# Once edited, reload systemd and restart LVBC daemon
sudo systemctl daemon-reload
sudo systemctl start lvbc
```


## Disable LVBC
```bash
# stop and/or disable LVBC systemd unit
sudo systemctl stop lvbc
sudo systemctl disable lvbc
sudo rm /etc/libvirt/hooks/qemu
```


## Technical docs

 - [Linux bridge vlan filtering](/doc/bvf.md)
 - [Persistent taps interfaces naming](/doc/ptn.md)
 - [Proper isolation for Linux Bridge](/doc/isol.md)
 - [Information for developers](/doc/dev.md)